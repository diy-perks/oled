import SSD1306
import time

import Gamepad

# Python 2/3 compatibility
try:
    input = raw_input
except NameError:
    pass

# ANSI colour code sequences
GREEN = '\033[0;32m'
CYAN = '\033[0;36m'
BLUE = '\033[1;34m'
RESET = '\033[0m'

if not Gamepad.available():
    print('Please connect your gamepad...')
    while not Gamepad.available():
        time.sleep(1.0)

print('Gamepad present')

gamepad = Gamepad.Gamepad()


bus = SSD1306.Bus()
disp = SSD1306.Display(bus)
disp.begin()
disp.clear()

x = 64
y = 32

disp.point(x,y)
disp.present()

try:
    while True:
        eventType, index, value = gamepad.getNextEvent()
        print(BLUE + eventType + RESET + ',\t  ' +
              GREEN + str(index) + RESET + ',\t' +
              CYAN + str(value) + RESET)
        if eventType == Gamepad.Gamepad.EVENT_AXIS:
            if index == 6:
                if value < 0:
                    x -= 1
                elif value > 0:
                    x += 1
                x = x % disp.width
                disp.point(x, y)
                disp.present()
            if index == 7:
                if value < 0:
                    y -= 1
                elif value > 0:
                    y += 1
                y = y % disp.height
                disp.point(x,y)
                disp.present()
        elif eventType == Gamepad.Gamepad.EVENT_BUTTON and index == 7:
            disp.clear()
            disp.present()
except KeyboardInterrupt:
    print("\nExiting...")
    disp.off()
