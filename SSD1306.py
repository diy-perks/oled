import smbus

class Bus:
    """Provides an abstraction of the actual I2C bus."""

    def __init__(self, address = 0x3c):
        self.i2c = smbus.SMBus(1) # newer version RASP (512 megabytes)
        self.address = address

    def init(self):
        """initialize hardware"""
        self.i2c.write_quick(self.address)

    def write(self, data):
        first = data[0]
        rest = data[1:]
        self.i2c.write_i2c_block_data(self.address, first, rest)

    def command(self, cmd, params = []):
        self.i2c.write_i2c_block_data(self.address, cmd, params)



class Display(object):
    """class for SSD1306 128x64 0.96inch OLED displays."""
        
    # Command Constants
    SETCONTRAST   = 0x81
    DISPLAYALLON_RESUME = 0xA4
    DISPLAYALLON  = 0xA5
    NORMALDISPLAY = 0xA6
    INVERTDISPLAY = 0xA7
    DISPLAYOFF    = 0xAE
    DISPLAYON     = 0xAF
    SETDISPLAYOFFSET = 0xD3
    SETCOMPINS    = 0xDA
    SETVCOMDETECT = 0xDB
    SETDISPLAYCLOCKDIV = 0xD5
    SETPRECHARGE  = 0xD9
    SETMULTIPLEX  = 0xA8
    SETLOWCOLUMN  = 0x00
    SETHIGHCOLUMN = 0x10
    SETSTARTLINE  = 0x40
    MEMORYMODE    = 0x20
    COLUMNADDR    = 0x21
    PAGEADDR      = 0x22
    COMSCANINC    = 0xC0
    COMSCANDEC    = 0xC8
    SEGREMAP      = 0xA0
    CHARGEPUMP    = 0x8D
    EXTERNALVCC   = 0x1
    SWITCHCAPVCC  = 0x2

    # Scrolling Constants
    ACTIVATE_SCROLL = 0x2F
    DEACTIVATE_SCROLL = 0x2E
    SET_VERTICAL_SCROLL_AREA = 0xA3
    RIGHT_HORIZONTAL_SCROLL = 0x26
    LEFT_HORIZONTAL_SCROLL = 0x27
    VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL = 0x29
    VERTICAL_AND_LEFT_HORIZONTAL_SCROLL = 0x2A


    def __init__(self, bus):
        self.bus = bus
        self.width = 128
        self.height = 64
        self._num_pages = int(self.height / 8) # Each byte encodes 8 rows in a column
        self._vccstate = Display.SWITCHCAPVCC
        self.clear()


    def command(self, cmd):
        """Send command byte to display"""
        self.bus.command(0x00, [cmd])


    def data(self, values):
        """Send bytes of data to display"""
        num = len(values)
        count = int((num + 31) / 32)
        for i in range(count):
            self.bus.command(0x40, values[32*i : min(num, 32*(i+1))])


    def needs_redraw(self):
        return any(self._dirty)


    def begin(self):
        """Initialize dispaly"""
        self.reset()

        self.command(Display.DISPLAYOFF)                    # 0xAE

        self.command(Display.SETDISPLAYCLOCKDIV)            # 0xD5
        self.command(0x80)                                  # the suggested ratio 0x80
                
        self.command(Display.SETMULTIPLEX)                  # 0xA8
        self.command(0x3F)

        self.command(Display.SETDISPLAYOFFSET)              # 0xD3
        self.command(0x0)                                   # no offset

        self.command(Display.SETSTARTLINE | 0x0)            # line #0

        self.command(Display.CHARGEPUMP)                    # 0x8D
        if self._vccstate == Display.EXTERNALVCC:
            self.command(0x10)
        else:
            self.command(0x14)
                
        self.command(Display.MEMORYMODE)                    # 0x20
        self.command(0x00)                                  # 0x0 act like ks0108        

        self.command(Display.SEGREMAP | 0x1)

        self.command(Display.COMSCANDEC)

        self.command(Display.SETCOMPINS)                    # 0xDA
        self.command(0x12)

        self.dim(False)

        self.command(Display.SETPRECHARGE)                  # 0xd9
        if self._vccstate == Display.EXTERNALVCC:
            self.command(0x22)
        else:
            self.command(0xF1)

        self.command(Display.SETVCOMDETECT)                 # 0xDB
        self.command(0x40)

        self.command(Display.DISPLAYALLON_RESUME)           # 0xA4

        self.command(Display.NORMALDISPLAY)                 # 0xA6

        self.on()


    def reset(self):
        """Reset the display"""
        pass # There's no RESET line on the I2C display


    def on(self):
        self.command(Display.DISPLAYON)


    def off(self):
        self.command(Display.DISPLAYOFF)


    def clear(self):
        """Clear contents of image buffer"""
        self._buffer = [ [0] * self.width for page in range(self._num_pages) ]
        self._dirty = [True] * self._num_pages


    def present(self):
        """Write display buffer to physical display"""
        if not self.needs_redraw():
            return

        buffer = self._buffer # In case it changes

        self.command(Display.COLUMNADDR)
        self.command(0)                  # Column start address
        self.command(self.width-1)       # Column end address
        for page in range(self._num_pages):
            if self._dirty[page]:
                self.command(Display.PAGEADDR)
                self.command(page)               # Page start address
                self.command(page)               # Page end address
                self.data(buffer[page])          # Write buffer data
                self._dirty[page] = False

    def invert(self, invert = True):
        if invert:
            self.command(Display.INVERTDISPLAY)
        else:
            self.command(Display.NORMALDISPLAY)


    def point(self, x, y, b = 1):
        """Draw a point at the specified position with the given brigtness (b = 1 -> on; b = 0 -> off)"""
        p = self.get_point(x, y)
        if p == b:
            return

        page = int(y/8)
        self._dirty[page] = True
        bit = y % 8
        if b == 0:
            self._buffer[page][x] &= ~(1 << bit)
        else:
            self._buffer[page][x] |= (1 << bit)


    def get_point(self, x, y):
        page = int(y/8)
        bit = y % 8
        return 1 if self._buffer[page][x] & (1 << bit) != 0 else 0


    def image(self, image):
        """Set buffer to value of Python Imaging Library image."""
        if image.mode != '1':
            raise ValueError('Image must be in mode 1.')
        imwidth, imheight = image.size
        if imwidth != self.width or imheight != self.height:
            raise ValueError('Image must be same dimensions as display ({0}x{1}).' .format(self.width, self.height))

        pix = image.load()
        # Iterate through the memory pages
        index = 0
        for page in range(self._num_pages):
            # Iterate through all x axis columns.
            for x in range(self.width):
                # Set the bits for the column of pixels at the current position.
                bits = 0
                # Don't use range here as it's a bit slow
                for bit in [0, 1, 2, 3, 4, 5, 6, 7]:
                    bits = bits << 1
                    bits |= 0 if pix[(x, page*8+7-bit)] == 0 else 1
                    # Update buffer byte and increment to next byte.
                self._buffer[index] = bits
                index += 1


    def set_contrast(self, contrast):
        """Sets the contrast of the display.
           Contrast should be a value between 0 and 255."""
        if contrast < 0 or contrast > 255:
            raise ValueError('Contrast must be a value from 0 to 255).')
        self.command(Display.SETCONTRAST)
        self.command(contrast)


    def dim(self, dim = True):
        """Adjusts contrast to dim the display if dim is True, 
           otherwise sets the contrast to normal brightness if dim is False."""
        # Assume dim display.
        contrast = 0
        # Adjust contrast based on VCC if not dimming.
        if not dim:
            if self._vccstate == Display.EXTERNALVCC:
                contrast = 0x9F
            else:
                contrast = 0xCF
        self.set_contrast(contrast)
